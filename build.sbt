name := "activator-play-slick"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.5" // or "2.10.4"

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "com.typesafe.play" %% "play-slick" % "0.8.1",
  "jp.t2v" %% "play2-auth"      % "0.13.5"
)

libraryDependencies += "org.xerial" % "sqlite-jdbc" % "3.8.11.2"

fork in Test := false

lazy val root = (project in file(".")).enablePlugins(PlayScala)