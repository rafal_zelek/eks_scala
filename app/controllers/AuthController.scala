package controllers

import jp.t2v.lab.play2.auth.{AuthElement, LoginLogout}
import models.Account
import play.api.data.Form
import play.api.mvc.{Action, Controller}
import views.html
import models._
import play.api._
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import play.api.Play.current
import play.api.mvc.BodyParsers._
import play.api.libs.json.Json
import play.api.libs.json.Json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object AuthController extends Controller with LoginLogout with AuthConfigImpl with AuthElement {

  /** Your application's login_form form.  Alter it to fit your application */
  val loginForm: Form[Option[Account]] = Form {
    mapping("id" -> number, "password" -> text)(Account.authenticate)(_.map(u => (u.id, u.password)))
      .verifying("Invalid email or password", result => result.isDefined)
  }

  /** Alter the login_form page action to suit your application. */
  def login = Action { implicit request =>
    Ok(html.login_form(loginForm))
  }

  /**
    * Return the `gotoLogoutSucceeded` method's result in the auth_successed action.
    *
    * Since the `gotoLogoutSucceeded` returns `Future[Result]`,
    * you can add a procedure like the following.
    *
    *   gotoLogoutSucceeded.map(_.flashing(
    *     "success" -> "You've been logged out"
    *   ))
    */
  def logout = Action.async { implicit request =>
    // do something...
    gotoLogoutSucceeded //Future.successful(Ok("wylogowales sie"))
  }

  def main = StackAction(AuthorityKey -> NormalUser) { implicit request =>
    val user = loggedIn

    Ok(html.auth_successed(user))
  }

  /**
    * Return the `gotoLoginSucceeded` method's result in the login_form action.
    *
    * Since the `gotoLoginSucceeded` returns `Future[Result]`,
    * you can add a procedure like the `gotoLogoutSucceeded`.
    */
  def authenticate = Action.async { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => Future.successful(Ok(html.login_form(formWithErrors, List("authentication failed")))),
      user => gotoLoginSucceeded(user.get.id)
    )
  }

}