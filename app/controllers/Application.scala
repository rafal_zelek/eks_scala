package controllers

import models._
import play.api._
import play.api.db.slick._
import play.api.db.slick.Config.driver.simple._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import play.api.Play.current
import play.api.mvc.BodyParsers._
import play.api.libs.json.Json
import play.api.libs.json.Json._

object Application extends Controller{

  //create an instance of the table
  val deparments = TableQuery[DepartmentTable] //see a way to architect your app in the computers-database-slick sample

  //JSON read/write macro
  implicit val deparmentsFormat = Json.format[Department]

  def index = DBAction { implicit rs =>
    Ok(views.html.index(deparments.list))
  }

  val catForm = Form(
    mapping(
      "id" -> number(),
      "shortName" -> text(),
      "fullName" -> text()
    )(Department.apply)(Department.unapply)
  )

  def insert = DBAction { implicit rs =>
    val cat = catForm.bindFromRequest.get
    deparments.insert(cat)

    Redirect(routes.Application.index)
  }

  def jsonFindAll = DBAction { implicit rs =>
    Ok(toJson(deparments.list))
  }

  def jsonInsert = DBAction(parse.json) { implicit rs =>
    rs.request.body.validate[Department].map { cat =>
        deparments.insert(cat)
        Ok(toJson(cat))
    }.getOrElse(BadRequest("invalid json"))    
  }
  
}
