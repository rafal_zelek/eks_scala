package models

import play.api.db.slick.Config.driver.simple._

import scala.concurrent.Future

case class Cat(name: String, color: String)

/* Table mapping
 */
class CatsTable(tag: Tag) extends Table[Cat](tag, "CAT") {

  def name = column[String]("name", O.PrimaryKey)

  def color = column[String]("color", O.NotNull)

  def * = (name, color) <>(Cat.tupled, Cat.unapply)
}

case class Account(id:Int, name:String, password:String, role:Role)
object Account{
  private val availableUsers =
    Account(1, "name", "password", NormalUser) ::
    Account(2, "name", "password", Administrator) ::
    Nil

  def findById(id:Int) = Future.successful(availableUsers.find(_.id == id))

  private def findUser(id: Int, password: String): Option[Account] = {
    availableUsers.find(u => u.id == id && u.password == password)
  }

  def authenticate(id: Int, password: String) = findUser(id, password)
}

trait Role
object Administrator extends Role
object NormalUser extends Role

case class Department(id: Int, shortName: String, fullName: String)
class DepartmentTable(tag: Tag) extends Table[Department](tag, "DEPARTMENT") {

  def id = column[Int]("id", O.PrimaryKey)

  def shortName = column[String]("shortName", O.NotNull)
  def fullName = column[String]("fullName", O.NotNull)

  def * = (id, shortName, fullName) <>(Department.tupled, Department.unapply)
}

case class Lecturer(id: Int, firstName: String, lastName: String, department: Department)

case class Course(id: Int, name: String, department: Department)

case class Term(id: Int, course: Course, name: String, thresholdPoint: Int)

case class Subject(id: Int, name: String, ectsValue: Int)

case class SubjectTerm(term: Term, subject: Subject)

case class Grade(int: Int, value: Int, student: Student, subject: Subject, lecturer: Lecturer)

case class SubjectLecturer(id: Int, lecturer: Lecturer, subject: Subject)

case class Student(id: Int)

case class StudentTerm(id: Int, student: Student, term: Term)